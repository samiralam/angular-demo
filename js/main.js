function renderByPlainJS() {


    var el = document.getElementById('githubUsername');
    var username = el.value;
    if (el.value === "") {
        alert("enter a username");
        return;
    }
    var apiUrl = "https://api.github.com/users/" + username + "/repos";

    // Plain Javascript starts
    function fetchJSONFile(path, callback) {

        var httpRequest = new XMLHttpRequest();

        httpRequest.onreadystatechange = function () {
            if (httpRequest.readyState === 4) {
                if (httpRequest.status === 200) {
                    var data = JSON.parse(httpRequest.responseText);
                    if (callback) callback(data);
                }
            }
        };

        httpRequest.open('GET', path);
        httpRequest.send();
    }

    //
    // this requires the url and executes a callback with the parsed result once
    fetchJSONFile(apiUrl, function (data) {

        // append the data to the user list
        for (var i in data) {
            var f = document.getElementById("plain-js-hook");
            var newNode = document.createElement("div");
            newNode.className = "well";
            f.appendChild(newNode);

            var txt =
                "<h3><a href='" + data[i].url + "'>" + data[i].name + "</a></h3>" +
                "<p>" +
                "<strong>Owner: </strong><a href='" + data[i].owner.html_url + "'>" + data[i].owner.login + "</a>" +
                "<img alt='' class='pull-right small-image' src='" + data[i].owner.avatar_url + "'/><br>" +
                "<strong>Updated at: </strong>" + data[i].pushed_at + "<br>" + "<span class='label label-primary'>" + data[i].language + "</span>" + "</p>";

            newNode.innerHTML = txt;
        }
    });

}

//
// With AngularJS Starts
var app = angular.module('app', []);

app.controller('reposController', function ($scope, $http) {

    $scope.renderByAngularJS = function () {

        var el = document.getElementById('githubUsername');
        var username = el.value;
        if (el.value === "") {
            alert("enter a username");
            return;
        }
        var apiUrl = "https://api.github.com/users/" + username + "/repos";

        $scope.title = "Repos";
        $http.get(apiUrl).success(function (data) {
            $scope.users = data; //setting the scope data
        }).error(function (data) {
            //handle errors
        });

    };

});